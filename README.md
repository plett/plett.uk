# Content for plett.uk

It all appears to work: https://plett.uk/ http://staging.plett.uk/

## Workflow for new posts
Note to self - it's probably been quite a while since you last wrote anything
for the website. These are the instructions you wrote last time you were here
and had to rediscover things.

### Git Branch
Make a new branch from master for your new post `git branch my-first-post master`. Thou shalt not commit straight to `master`.

### Make your post(s)
There is a script which runs hugo in a docker container and can be used as `./dhugo new posts/my-first-post.md`. If this is a newly checked out branch, you'll want the submodule for the theme `git submodules update`.

### Create your content
Write whatever inane babblings come to mind.

### Check it looks sensible locally
Use `./dhugoserver` to run `hugo server` in a docker container. Commit it to
your branch when you're done. 

### Merge it into master
`git checkout master`
`git merge my-first-post`

### Push it
`git push`
Through the magic of Gitlab CI, things will happen and the output from Hugo will
appear at https://plett.uk/

## Staging Site
When you're updating tool versions, or making config changes which might break
things, there is a staging branch with different CI magic to deploy to
http://staging.plett.uk/

The staging branch should be fast-forwardable to master, but probably isn't
because you are a terrible person. `git diff master..staging`. If you need to
blow away the staging branch and make it be master again, do `git checkout -B
staging master`. You will likely need to do a force push later because you're
rewriting history.

Make your changes in `staging`, push them to Gitlab, check the staging site
looks fine, merge them to `master`. Optionally, delete the staging branch
completely until you next need to use the staging site.

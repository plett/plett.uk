+++
draft = false
date = 2022-07-18T21:05:00Z
title = "Making a dumb air quality sensor smart"
description = "Making a dumb air quality sensor smart"
slug = ""
tags = []
categories = []
+++

IKEA make a very inexpensive (about £10) PM2.5 air quality sensor called
Vindriktning. There are only two things wrong with it - it's annoyingly noisy,
and the only output from it is a red/yellow/green LED traffic light of the
current air quality, no numerical values and definitely no history or graphs.
Let's try to fix both those problems.

<!--more-->

## Making it smart

Making it smart is actually easier than it sounds, and all the
[hard][styleoversubstance] [work][instructables] has [already][tasmota] [been
done][hassforum], including [support for the sensor][esphome-pm1006] in ESPHome
which is my platform of choice for small sensors to talk to Home Assistant. It
just involved soldering 3 wires between the standard Vindriktning board and a
Wemos D1 Mini and writing a short config file.

![A disassembled Vindriktning with a D1 Mini soldered to it](soldered-board.jpg "A modified sensor before re-assembly")

[styleoversubstance]: https://style.oversubstance.net/2021/08/diy-use-an-ikea-vindriktning-air-quality-sensor-in-home-assistant-with-esphome/
[instructables]: https://www.instructables.com/Connecting-a-IKEA-Vindriktning-to-Home-Assistant-U/
[tasmota]: https://blakadder.com/vindriktning-tasmota/
[hassforum]: https://community.home-assistant.io/t/ikea-vindriktning-air-quality-sensor/324599
[esphome-pm1006]: https://next.esphome.io/components/sensor/pm1006.html

## Testing the sensor

![Two modified sensors](assembled-units.jpg "Two smart air sensors")

I did the smart mod on two units at the same time so I could compare the two
together. While I can't calibrate one against the other, if the sensors in both
give similar values in the same circumstances then the measurements are at least
repeatable, even if they're not accurate. To do the comparison, I sat both units
next to the kitchen stove while I fried some halloumi and they matched each
other wonderfully.

![A graph showing very good correlation between the two sensors](halloumi-graph.png "Cooking considered harmful!")

The kitchen wasn't noticeably smoky, but it was clearly way more than the
sensors were intended to handle. The graph shows the values from the sensors
peaking far above 1000 µg/m³. I'm not sure if I believe the actual values or not - 
the US EPA's highest category for PM2.5 is from 250 to 500 µg/m³ is "Hazardous"
if exposed for 24 hours, with "serious risk of respiratory effects in general
population" and [a study][paper] found that Chinese-style cooking can
cause a value of PM2.5 of 160 µg/m³ in the kitchen. Perhaps being right next to
the stove meant that the samples weren't representative of the air in the rest
of the room, or perhaps these sensors really are only good for red/yellow/green
traffic light indicators.

[paper]: https://www.sciencedirect.com/science/article/abs/pii/S1352231011008600

I looked at figures for my town from [official monitoring stations][uk-air]
and they say the PM2.5 around here is under 10 µg/m³, which matches what the
IKEA sensors say when I'm not cooking, so I'm willing to believe that the
sensors are at least in the right ballpark.

[uk-air]: https://uk-air.defra.gov.uk

## Making it quiet

Every review of these sensors mentions that they are noisy. They have a fan in
them to draw a sample of air through the sensor, and they run the fan on a duty
cycle of 20 seconds on and 10 seconds off - so it's constantly ramping up and
down and is infuriatingly  noticeable. Many people on the internet have [tried
undervolting][hypfer3v3] the fan to reduce noise.

The IKEA circuit switches the fan on and off using the negative fan wire, so you
can supply the positive fan wire with a lower supply voltage and it will still
cycle up and down when taking samples. The 3.3V regulator in the Wemos D1 Mini
board I'm using will easily drive the fan, so I connected it there.

[hypfer3v3]: https://github.com/Hypfer/esp8266-vindriktning-particle-sensor#low-noise-mod

![The 3.3 volt modification](3v3-mod-1.jpg "The fan's power now comes from the
D1's 3.3v regulator")

The result is almost entirely silent. The fan still spins up and down, and I can
hear it if I hold the sensor up to my ear, but it is completely inaudible at
arms' length. But how broken are the sensor values now? Since I have two sensors
and have only modified one of them, I can repeat the cooking test and see how
the two differ now, and calculate values for a filter in ESPHome to bring the
readings back up to where they should be.

But I need not have worried - if the lower voltage does affect the readings at
all, it's by less than 10%, so I don't need to scale the values up at all.

![a graph showing good correlation between 3.3v and 5v fan speeds](3v3-5v-compare-graph.png "Fan voltages of 3.3v (blue) and 5v (purple) while grilling sausages")

## Smoothing out spurious values

The sensors seem quite prone to what I am assuming are false readings. They can
be giving a reading of under 5 µg/m³ for hours, and then suddenly spike to 100
µg/m³ and then drop back to 5 a few seconds later. I am assuming a single large
particle got into the sensor and it interpreted it as many small particles. The
[blog post][styleoversubstance] which I took most of the esphome config from
used a `sliding_window_moving_average` filter in esphome to flatten out this
noise, but I found it not to be sufficient. Switching that to a
[quantile][quantilefilter] filter improved the noise dramatically.

[quantilefilter]: https://esphome.io/components/sensor/index.html#quantile

## Is it worth it?

Maybe. For under £15 in parts and a few hours of my time, I now have an air
quality sensor pushing data into Home Assistant. This is the first time I've
looked into air quality and it turns out that my air is pretty clean, at least
as far as PM2.5 is concerned. It's nice to have the data but it's not actionable
for me - I don't have any air filters which I can operate automatically when the
figures get high, and I don't feel a need to get any. Maybe siting one of the
sensors outside would prove useful, but for now the data is just another number
in a dashboard.


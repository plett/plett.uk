+++
draft = false
date = 2020-11-01T23:30:22Z
title = "A USB Quinkey chording keyboard"
description = "Using a 1980s chording keyboard as a modern USB input device"
slug = ""
tags = []
categories = []
+++

In about 1990, I recovered a Quinkey chording keyboard which was being thrown
out by my school. For the unfamiliar, a chording keyboard is a text entry
keyboard where there are fewer buttons than there are letters (typically only
one button for each finger) and you press multiple buttons at once to type a
character. Their proponents will tell you that they are much more ergonomic and
faster to type on than the QWERTY keyboards we have become used to.

![My Quinkey](quinkey.jpg)

One of the early electronic chording keyboards was used in a Microwriter in the
late 1970s to early 1980s. The MicroWriter was a standalone, battery powered
chording keyboard with a few KB of built in memory and a small LCD display. You
could use it to write documents while on the move and then attach it to a
printer when you got to the office. You wrote on it using these combinations of
keypresses:

![Microwriter alphabet chord diagrams](Microwriter%20Alphabet.png)

A Quinkey is a cut-down Microwriter without the display or storage electronics
inside it and was intended as a text input device for a BBC B microcomputer
using the same chord patterns. It was possible to connect four Quinkeys up to a
single BBC B and have multiple people operating the one computer, each with a
quarter of the screen to themselves. It was marketed to schools as a way to get
more kids using computers without needing to buy more computers, which probably
explains why my school had one. Chording keyboards have a steep learning curve
to get past before you can actually enter any text, and very few programs on the
BBC Micro ever supported the Quinkey - which probably also explains why I had no
idea they even existed until I found one in the bin.

Thanks to the awesome people at archive.org, you can read [a review of the
Quinkey][review] from a [1984 issue of Acorn User][acornuser]. It's a very
technical review, and goes into incredible detail about the inner workings of
the Quinkey and its bundled software.

[review]: Quinkey%20Review%20-%20Acorn%20User%20026%201984-09.pdf
[acornuser]: https://archive.org/details/Acorn_User_Number_026_1984-09_Redwood_Publishing_GB

I had a BBC Micro at home, but I didn't have the interface box which connected a
Quinkey to it - so it was fairly useless to me. But I didn't want to throw it
away, so I put it in a cupboard in my parents house and forgot about it for
about 20 years, until I had a house of my own and my mother, quite
understandably, wanted me to move all my junk out of hers.

So I then had a Quinkey which was taking up space in *my* house, and a desire to
actually see what it was like to type on it. I also had a BBC B in the loft, but
still not the interface box to join the two together, so the first idea was to
re-build the interface and somehow acquire some software which could talk to it.
After discovering that the interface attached 4 of them to the analogue port, I
had a hunch that it was quite simple inside, and all the cleverness was
happening in the software. A multimeter confirmed that it was a simple resistor
ladder inside the Quinkey to generate an analogue voltage which the BBC must
then have sampled.

And from that, a plan was formed - I would do the same sampling using an ADC
on a microcontroller and turn it into a USB HID keyboard. I could then use it
on my everyday computer, learn how to type on it and see if chording keyboards
are worth the learning curve.

There was a [Hacksoton][] event in 2014, where people build things on the day
and then do a show and tell at the end, and I didn't have any other projects
that needed doing. With some cutting of corners, it was possible to solder
up some interfacing hardware, [write some Arduino code][github] and memorise
enough of the chords to type on it during the 6 or so hours that the event ran.
The longest part of the process was manually translating the chording patterns
into a binary representation for the Arduino and mapping those to an ASCII
character.

[Hacksoton]: https://www.hacksoton.com/
[github]: https://github.com/plett/quinkey
[schematic]: # (needs a schematic diagram here)

I was able to take my laptop up on stage at the end of the day with the Quinkey
and the hastily constructed electronics on a prototyping board and give an
explanation of what I'd been doing all day, and use the Quinkey to type the word
"hacksoton" into a text editor on my laptop.

No recordings exist of that, but I've put a short video together to demonstrate
how to type on it.

{{< video src="quinkey-usage" >}}

Was it worth it? Are chording keyboards useful?

I think they are a neat idea and I'm happy that I've made this work, but
chording keyboards for mobile use are clearly from a pre-touchscreen world. The
use-case of a Microwriter where a businessman writes his memos on on it during
his morning train ride into the office and then connects it to a printer when he
gets there has been completely obsoleted by smartphones and laptops. I'm glad
I've got a Quinkey, but I view it more as a historical artifact than a useful
piece of tech.

---
title: "About Paul Lettington"
date: 2018-10-28T22:48:55Z
---

I am an infrastructure nerd with a particular interest in network technology. I
have been running internet BGP for access ISPs and content networks since 2006
and have so far managed to avoid breaking the Internet. I am also an Amateur
Radio geek and a lapsed member of my local Hackspace.

This website exists mostly as an excuse to see exactly how hard it is to build a
continuous deployment workflow which turns a git push into a published web page.
As such, there is hardly any content and the [toolchain behind it][toolchain] is
over-engineered!

[toolchain]: {{< ref "posts/this-site" >}}
